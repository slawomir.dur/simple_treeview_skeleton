#include "treeview.h"

TreeView::TreeView(DataModel* model, QWidget* parent) : QTreeView(parent)
{
    setModel(model);
    setAlternatingRowColors(true);
    setHeaderHidden(true);
    setColumnWidth(0, 180);
    resizeColumnToContents(1);
    expandAll();
}
