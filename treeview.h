#ifndef TREEVIEW_H
#define TREEVIEW_H

#include <QTreeView>
#include "datamodel.h"

class TreeView : public QTreeView
{
public:
    TreeView(DataModel* model, QWidget* parent = nullptr);
};

#endif // TREEVIEW_H
