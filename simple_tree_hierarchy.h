#ifndef SIMPLE_TREE_HIERARCHY_H
#define SIMPLE_TREE_HIERARCHY_H

#include <list>
#include <QString>
#include <QStringList>
#include <QJsonValue>
#include <QVariant>
#include <limits>

class BaseElement;
class EnumElement;
class NumberElement;

class HierarchyElementFactory
{
public:
    HierarchyElementFactory() = delete;
    static BaseElement* createElement(const QString& type);
};

class ElementPopulator
{
public:
    ElementPopulator() = delete;
    static void populateElement(const QJsonObject& json, BaseElement* element);
private:
    static void populateEnum(const QJsonObject& json, EnumElement* enumElement);
    static void populateNumber(const QJsonObject& json, NumberElement* numberElement);
};

class BaseElement
{
protected:
    QVariant data {};
    QString type;
    QString name;
    QVariant default_data {};
    BaseElement* parent = nullptr;
public:
    BaseElement(const QString& type = "Base") : type(type) {}
    virtual ~BaseElement() { delete this; }
    const QString& getName() const { return name; }
    QString getName() { return name; }
    void setName(const QString& new_name) { name = new_name; }
    virtual QString getType() const { return type; }
    virtual QVariant getData() const { return data; }
    QVariant getDefault() const { return default_data; }
    void setDefault(const QVariant& defaultOption) { default_data = defaultOption; }
    BaseElement* getParent() { return parent; }
    void setParent(BaseElement* parent) { this->parent = parent; }
    BaseElement* root();
    int level();
    virtual QJsonValue toJson() const;
};

class TreeElement : public BaseElement
{
private:
    std::list<BaseElement*> sons;
  public:
    TreeElement() : BaseElement("Tree") {}
    virtual QVariant getData() const { return QString::number(sons.size()); }
    int sonsNumber() { return sons.size(); }
    BaseElement* getSon(const int& index);
    std::list<BaseElement*>::iterator begin() { return sons.begin(); }
    std::list<BaseElement*>::iterator end() { return sons.end(); }
    std::list<BaseElement*>::const_iterator cbegin() const { return sons.cbegin(); }
    std::list<BaseElement*>::const_iterator cend() const { return sons.cend(); }
    void append(BaseElement* el) { sons.push_back(el); el->setParent(this);}
    virtual QJsonValue toJson() const;
};

class EnumElement : public BaseElement
{
    QStringList options;
    QString defaultOption;
public:
    friend class ElementPopulator;
    EnumElement() : BaseElement("Enum") {}
    EnumElement(const QStringList& options);
    const QStringList& getOptions() const { return options; }
    QStringList getOptions() { return options; }
    //virtual QVariant getDefault() const { return defaultOption; }
    virtual QJsonValue toJson() const;
private:
    void setOptions(const QStringList& new_options) { options = new_options; }
};

class EditableElement : public BaseElement
{
protected:
    EditableElement(const QString& type) : BaseElement(type) {}
};

class LongStringElement : public EditableElement
{
public:
    LongStringElement() : EditableElement("Long String") {}
};

class StringElement : public EditableElement
{
public:
    StringElement() : EditableElement("String") {}
};

class NumberElement : public EditableElement
{
protected:
    double max { std::numeric_limits<double>::max() };
    double min { std::numeric_limits<double>::min() };
    double step {};
    double defaultValue {};
protected:
    NumberElement(const QString& type) : EditableElement(type) {}
public:
    double getMax() const { return max; }
    double getMin() const { return min; }
    double getStep() const { return step; }
    //virtual QVariant getDefault() const { return defaultValue; }
    friend class ElementPopulator;
};

class IntElement : public NumberElement
{
public:
    IntElement() : NumberElement("Int") {}
};

class DoubleElement : public NumberElement
{
public:
    DoubleElement() : NumberElement("Double") {}
};

class BooleanElement : public BaseElement
{
public:
    BooleanElement() : BaseElement("Boolean") {}
};


#endif // SIMPLE_TREE_HIERARCHY_H
