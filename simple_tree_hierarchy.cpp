#include "simple_tree_hierarchy.h"
#include <QDebug>
#include <QJsonObject>
#include <QJsonArray>

BaseElement* HierarchyElementFactory::createElement(const QString &type)
{
    if (type == "tree") return new TreeElement();
    else if (type == "enum") return new EnumElement();
    else if (type == "string") return new StringElement();
    else if (type == "longstring") return new LongStringElement();
    else if (type == "int") return new IntElement();
    else if (type == "double") return new DoubleElement();
    else if (type == "boolean") return new BooleanElement();
    else throw std::runtime_error("Unknown hierarchy element type.");
}

void ElementPopulator::populateElement(const QJsonObject &json, BaseElement *element)
{
    element->setDefault(json["default"].toVariant());
    element->setName(json["name"].toString());
    if (auto enumElement = dynamic_cast<EnumElement*>(element))
    {
        populateEnum(json, enumElement);
    }
    else if (auto numberElement = dynamic_cast<NumberElement*>(element))
    {
        populateNumber(json, numberElement);
    }
}

void ElementPopulator::populateEnum(const QJsonObject& json, EnumElement* enumElement)
{
    QStringList optionList;
    for (const QJsonValue& option: json["options"].toArray())
    {
        if (option.isString())
            optionList << option.toString();
        else if (option.isDouble())
            optionList << QString::number(option.toDouble());
    }
    enumElement->setOptions(optionList);
}

void ElementPopulator::populateNumber(const QJsonObject& json, NumberElement* numberElement)
{
    if (json.contains("min"))
    {
        numberElement->min = json["min"].toDouble();
    }

    if (json.contains("max"))
    {
        numberElement->max = json["max"].toDouble();
    }

    if (json.contains("step"))
    {
        numberElement->step = json["step"].toDouble();
    }

    /*if (json.contains("default"))
    {
        numberElement->setDefault(json["default"].toVariant());
    }
    else numberElement->defaultValue = 0.0;
    */
}

BaseElement* BaseElement::root()
{
    BaseElement* root = this;
    while (root->getParent() != nullptr)
    {
        root = getParent();
    }
    return root;
}

int BaseElement::level()
{
    BaseElement* root = this;
    int level {};
    while (root->getParent() != nullptr)
    {
        root = getParent();
        ++level;
    }
    return level;
}

QJsonValue BaseElement::toJson() const
{
    return QJsonValue();
}

BaseElement* TreeElement::getSon(const int &index)
{
    auto it = sons.begin();
    for (int i = 0; i < index; ++i)
    {
        std::next(it);
    }
    return *it;
}

QJsonValue TreeElement::toJson() const
{
    return QJsonValue();
}

EnumElement::EnumElement(const QStringList& options) : BaseElement("Enum"), options(options) {}

QJsonValue EnumElement::toJson() const
{
    return QJsonValue();
}
