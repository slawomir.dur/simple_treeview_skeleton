TARGET=TreeViewProject

TEMPLATE=app

QT=core gui

greaterThan(QT_MAJOR_VERSION, 4): QT+=widgets

SOURCES += \
    main.cpp \
    datamodel.cpp \
    treeview.cpp \
    customdelegate.cpp \
    simple_tree_hierarchy.cpp \
    jsonparser.cpp

HEADERS += \
    datamodel.h \
    treeview.h \
    customdelegate.h \
    simple_tree_hierarchy.h \
    jsonparser.h

DISTFILES += \
    gui_json.json

