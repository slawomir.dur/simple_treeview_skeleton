#include <QApplication>
#include "customdelegate.h"
#include "datamodel.h"
#include "treeview.h"
#include "jsonparser.h"


int main(int argv, char** argc)
{
    QApplication app(argv, argc);

    const TreeElement* root_ { JsonParser::createTreeFromJson("gui_json.json") };

    DataModel model(root_);
    TreeView *treeView = new TreeView(&model);

    CustomDelegate delegate;
    treeView->setItemDelegate(&delegate);

    treeView->show();


    return app.exec();
}
