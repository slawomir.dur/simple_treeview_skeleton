#ifndef DATAMODEL_H
#define DATAMODEL_H

#include <QStandardItemModel>
#include <QStandardItem>
#include <QTreeView>
#include <QJsonValue>
#include <QJsonObject>
#include <QThread>
#include <functional>
#include "simple_tree_hierarchy.h"


class DataModel : public QStandardItemModel
{
    Q_OBJECT
private:
    QThread jsonCreationThread;
public:
    DataModel(const TreeElement* root, QObject* parent = nullptr);
    ~DataModel() { jsonCreationThread.quit(); jsonCreationThread.wait(); }
    void buildModel(QStandardItem* rootItem, const BaseElement* element);
    QJsonObject toJson() const;
private:
    void configureJsonThread();
signals:
    void configurationChanged(QJsonValue json);
    void operate(DataModel* model);
public slots:
    void onDataChanged();
    void emitConfiguration(QJsonObject json);
};



class DataItem : public QStandardItem
{
protected:
    const BaseElement* element = nullptr;
    const QStandardItem* sibling = nullptr;
public:
    static QList<QStandardItem*> createItemList(const BaseElement* element);
    static DataItem* createItem(const QStandardItem* sibling, const BaseElement* element);
    const QStandardItem* getSibling() const { return sibling; }
    const QStandardItem* getSibling() { return sibling; }
    virtual QJsonValue toJson() const;
protected:
    DataItem(const QStandardItem* sibling, const BaseElement* element);
};

class JsonCreator : public QObject
{
    Q_OBJECT
public:
    JsonCreator(QObject *parent = nullptr) : QObject(parent) {}

public slots:
    void createJson(DataModel* model) {
        QModelIndex root_data_index { model->index(0, 1) };
        DataItem* root_item_data { static_cast<DataItem*>(model->itemFromIndex(root_data_index)) };
        QJsonObject json = root_item_data->toJson().toObject();
        emit jsonReady(json);
    }

signals:
    void jsonReady(const QJsonObject& json);
};

class TreeItem : public DataItem
{
public:
    TreeItem(const QStandardItem* sibling, const BaseElement* element) : DataItem(sibling, element) {}
    virtual QJsonValue toJson() const;
};

class EnumItem : public DataItem
{
public:
    EnumItem(const QStandardItem* sibling, const BaseElement* element) : DataItem(sibling, element) {}
    QStringList getOptions() { return dynamic_cast<const EnumElement*>(element)->getOptions(); }
};

class LongStringItem : public DataItem
{
  public:
    LongStringItem(const QStandardItem* sibling, const BaseElement* element) : DataItem(sibling, element) {}
};

class StringItem : public DataItem
{
public:
    StringItem(const QStandardItem* sibling, const BaseElement* element) : DataItem(sibling, element) {}
};

class NumberItem : public DataItem
{
  public:
    NumberItem(const QStandardItem* sibling, const BaseElement* element) : DataItem(sibling, element) {}
    double getMin() { return static_cast<const NumberElement*>(element)->getMin(); }
    double getMax() { return static_cast<const NumberElement*>(element)->getMax(); }
    double getStep() { return static_cast<const NumberElement*>(element)->getStep(); }
    double getDefault() { return static_cast<const NumberElement*>(element)->getDefault().toDouble(); }
};

class IntItem : public NumberItem
{
public:
    IntItem(const QStandardItem* sibling, const BaseElement* element) : NumberItem(sibling, element) {}
};

class DoubleItem : public NumberItem
{
public:
    DoubleItem(const QStandardItem* sibling, const BaseElement* element) : NumberItem(sibling, element) {}
};

class BooleanItem : public DataItem
{
public:
    BooleanItem(const QStandardItem* sibling, const BaseElement* element) : DataItem(sibling, element) {}
};

#endif // DATAMODEL_H
