#include "jsonparser.h"
#include <QDebug>
#include <QJsonArray>
#include <memory>

const TreeElement* JsonParser::createTreeFromJson(QString filename)
{
    QJsonDocument json_document { loadJson(filename) };
    QJsonObject json_object { json_document.object() };

    TreeElement* root { new TreeElement() };
    buildTree(json_object, root);

    return root;
}

QJsonDocument JsonParser::loadJson(QString filename)
{
    QFile file;
    file.setFileName(filename);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        throw std::runtime_error("Cannot open " + filename.toStdString() + " json file.");
    }
    QString jsonText { file.readAll() };
    file.close();

    return QJsonDocument(QJsonDocument::fromJson(jsonText.toUtf8()));
}

void JsonParser::buildTree(const QJsonObject& jsonObject, TreeElement* root)
{
    QString type { jsonObject["type"].toString() };
    BaseElement* element { HierarchyElementFactory::createElement(type) };
    ElementPopulator::populateElement(jsonObject, element);
    root->append(element);
    if (auto tree = dynamic_cast<TreeElement*>(element))
    {
        for (const QJsonValue& son: jsonObject["sons"].toArray())
        {
            buildTree(son.toObject(), tree);
        }
    }
}
