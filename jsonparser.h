#ifndef JSONPARSER_H
#define JSONPARSER_H

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QFile>
#include "simple_tree_hierarchy.h"

class JsonParser
{
public:
    JsonParser() = default;
    static const TreeElement* createTreeFromJson(QString filename);
    static QJsonDocument loadJson(QString filename);
    static void buildTree(const QJsonObject& jsonObject, TreeElement* tree);
};

#endif // JSONPARSER_H
