#include "customdelegate.h"
#include <QLineEdit>
#include <QDebug>
#include <QStandardItemModel>
#include <QComboBox>
#include <QLineEdit>
#include <QCheckBox>
#include <QDoubleSpinBox>
#include <QStringListModel>
#include <QMessageBox>
#include <QTextEdit>
#include "datamodel.h"

CustomDelegate::CustomDelegate(QObject *parent) : QStyledItemDelegate(parent)
{

}

QWidget* CustomDelegate::createEditor(QWidget* parent, const QStyleOptionViewItem& /*option*/, const QModelIndex& index) const
{
    // this is just a simplification
    const QStandardItemModel* standardModel { static_cast<const QStandardItemModel*>(index.model()) };
    QStandardItem* item { standardModel->itemFromIndex(index) };

    if (auto enumElem = dynamic_cast<EnumItem*>(item))
    {
        QComboBox* box { new QComboBox(parent) };
        QStringListModel* model { new QStringListModel(parent) };
        model->setStringList(enumElem->getOptions());
        box->setModel(model);
        return box;
    }
    else if (auto stringElem = dynamic_cast<StringItem*>(item))
    {
        return new QLineEdit(parent);
    }
    else if (dynamic_cast<LongStringItem*>(item))
    {
        QTextEdit *textEdit { new QTextEdit(parent) };
        return textEdit;

    }
    else if (auto numElem = dynamic_cast<NumberItem*>(item))
    {
        QDoubleSpinBox* numBox { new QDoubleSpinBox(parent) };
        numBox->setMinimum(numElem->getMin());
        numBox->setMaximum(numElem->getMax());
        numBox->setValue(numElem->getDefault());
        numBox->setSingleStep(numElem->getStep());
        return numBox;
    }
    else if (auto boolElem = dynamic_cast<BooleanItem*>(item))
    {
        return new QCheckBox(parent);
    }
    else
    {
        QMessageBox::information(parent, "Information", "This element cannot be edited.", QMessageBox::Ok);
    }
}

void CustomDelegate::setEditorData(QWidget* editor, const QModelIndex& index) const
{
    const QStandardItemModel* standardModel { static_cast<const QStandardItemModel*>(index.model()) };
    QStandardItem* item { standardModel->itemFromIndex(index) };
    if (dynamic_cast<EnumItem*>(item))
    {
        QComboBox* box { static_cast<QComboBox*>(editor) };
        box->setCurrentText(item->data(Qt::DisplayRole).toString());
    }
    else if (dynamic_cast<NumberItem*>(item))
    {
        QDoubleSpinBox* numBox { static_cast<QDoubleSpinBox*>(editor) };
        numBox->setValue(item->data(Qt::DisplayRole).toDouble());
    }
    else if (dynamic_cast<LongStringItem*>(item))
    {
        QTextEdit* textEdit { static_cast<QTextEdit*>(editor) };
        textEdit->setText(item->data(Qt::DisplayRole).toString());
    }
    else if (dynamic_cast<BooleanItem*>(editor))
    {
        // to be repaired!
        QCheckBox* checkBox { static_cast<QCheckBox*>(editor) };
        checkBox->setChecked(item->data(Qt::DisplayRole).toBool());
    }
}

void CustomDelegate::setModelData(QWidget* editor, QAbstractItemModel* model, const QModelIndex& index) const
{
    const QStandardItemModel* standardModel { static_cast<const QStandardItemModel*>(index.model()) };
    QStandardItem* item { standardModel->itemFromIndex(index) };
    if (dynamic_cast<LongStringItem*>(item))
    {
        QTextEdit* textEdit { static_cast<QTextEdit*>(editor) };
        item->setData(textEdit->toPlainText(), Qt::DisplayRole);
    }
    else QStyledItemDelegate::setModelData(editor, model, index);
}

void CustomDelegate::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& /*index*/) const
{
    editor->setGeometry(option.rect);
}


