#include "datamodel.h"
#include <QItemSelectionModel>
#include <QDebug>
#include <QJsonObject>
#include <QJsonDocument>
#include <memory>

DataModel::DataModel(const TreeElement* root, QObject* parent) : QStandardItemModel(parent)
{
    QStandardItem* invisibleRoot { invisibleRootItem() };
    buildModel(invisibleRoot, root);
    connect(this, SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)), this, SLOT(onDataChanged()));
    configureJsonThread();
}

void DataModel::buildModel(QStandardItem *rootItem, const BaseElement *element)
{
    QList<QStandardItem*> itemList { DataItem::createItemList(element) };
    rootItem->appendRow(itemList);
    if (auto tree = dynamic_cast<const TreeElement*>(element))
    {
        for (auto it = tree->cbegin(); it != tree->cend(); ++it)
        {
            buildModel(itemList.first(), *it);
        }
    }
}

QJsonObject DataModel::toJson() const
{
    QModelIndex root_data_index { index(0, 1) };
    DataItem* root_item_data { static_cast<DataItem*>(itemFromIndex(root_data_index)) };
    return root_item_data->toJson().toObject();
}

void DataModel::configureJsonThread()
{
    JsonCreator* creator = new JsonCreator();
    creator->moveToThread(&jsonCreationThread);
    connect(&jsonCreationThread, &QThread::finished, creator, &QObject::deleteLater);
    connect(this, &DataModel::operate, creator, &JsonCreator::createJson);
    connect(creator, &JsonCreator::jsonReady, this, &DataModel::emitConfiguration);
    jsonCreationThread.start();
}

void DataModel::onDataChanged()
{  
    emit operate(this);
}

void DataModel::emitConfiguration(QJsonObject json)
{
    emit configurationChanged(json);
}


DataItem::DataItem(const QStandardItem* sibling, const BaseElement *element) : element(element), sibling(sibling)
{
    QVariant data { element->getData() };
    setData(element->getDefault(), Qt::DisplayRole);
}

QList<QStandardItem*> DataItem::createItemList(const BaseElement *element)
{
    qDebug() <<  "DataItem::createItemList " << element->getType();
    QStandardItem *labelItem { new QStandardItem() };
    labelItem->setData(element->getName(), Qt::DisplayRole);

    QStandardItem *realItem { DataItem::createItem(labelItem, element) };
    return { labelItem, realItem };
}

DataItem* DataItem::createItem(const QStandardItem* sibling, const BaseElement *element)
{
    if (dynamic_cast<const EnumElement*>(element))
    {
        return new EnumItem(sibling, element);
    }
    else if (dynamic_cast<const LongStringElement*>(element))
    {
        return new LongStringItem(sibling, element);
    }
    else if (dynamic_cast<const StringElement*>(element))
    {
        return new StringItem(sibling, element);
    }
    else if (dynamic_cast<const IntElement*>(element))
    {
        return new IntItem(sibling, element);
    }
    else if (dynamic_cast<const DoubleElement*>(element))
    {
        return new DoubleItem(sibling, element);
    }
    else if (dynamic_cast<const BooleanElement*>(element))
    {
        return new BooleanItem(sibling, element);
    }
    else if (dynamic_cast<const TreeElement*>(element))
    {
        return new TreeItem(sibling, element);
    }
    else
    {

    }
}

QJsonValue DataItem::toJson() const
{
    QVariant data { QStandardItem::data(Qt::DisplayRole) };
    return QJsonValue(QJsonValue::fromVariant(data));
}

QJsonValue TreeItem::toJson() const
{
    QStandardItemModel* model { this->model() };
    QJsonObject object;
    QModelIndex sibling_index { model->indexFromItem(this->getSibling()) };

    int children { model->rowCount(sibling_index) };
    for (int i = 0; i < children; ++i)
    {
        QModelIndex child_label_index { model->index(i, 0, sibling_index) };
        QModelIndex child_data_index { model->index(i, 1, sibling_index) };
        DataItem* child_item { static_cast<DataItem*>(model->itemFromIndex(child_data_index)) };
        object.insert(child_label_index.data().toString(), child_item->toJson());
    }
    return object;
}



